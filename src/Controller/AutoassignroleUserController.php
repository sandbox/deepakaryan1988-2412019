<?php

namespace Drupal\autoassignrole\Controller;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;

class AutoassignroleUserController extends ConfigFormBase {

  public function getFormId() {
    return 'autoassignrole_user_settings_config_form';
  }

  protected function getEditableConfigNames() {
    return ['autoassignrole_user_settings.config'];
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('autoassignrole_user_settings.config');
    $form['autoassignrole_user_active'] = [
      '#type' => 'radios',
      '#title' => $this->t('User role assignment'),
      '#default_value' => $config->get('autoassignrole_user_active', 0),
      '#description' => $this->t('Toggles allowing end users to select roles when creating their accounts.'),
      '#options' => [1 => $this->t('Enabled'), 0 => $this->t('Disabled')],
   ];

    // We can disregard the authenticated user role since it is assigned to each
    // user by Drupal.
    $roles = user_role_names(TRUE);
    unset($roles[DRUPAL_AUTHENTICATED_RID]);

    if ($roles) {
      $form['autoassignrole_user_roles'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Roles'),
        '#default_value' => $config->get('autoassignrole_user_roles', NULL),
        '#description' => $this->t('Check the specific roles the user will have the option of choosing. The Authenticated User role is automatically assigned by Drupal core and can not be edited.'),
        '#options' => $roles,
        '#states' => [
          'visible' => [
            ':input[name="autoassignrole_user_active"]' => ['value' => 1],
          ],
        ],
      ];
    }

    $form['autoassignrole_user_multiple'] = [
      '#type' => 'radios',
      '#title' => $this->t('User role selection'),
      '#default_value' => $config->get('autoassignrole_user_multiple', 0),
      '#description' => $this->t('Should the end user be allowed to choose a single role or can they choose multiple roles?'),
      '#options' => [0 => $this->t('Single role'), 1 => $this->t('Multiple roles')],
      '#states' => [
        'visible' => [
          ':input[name="autoassignrole_user_active"]' => ['value' => 1],
        ],
      ],
    ];

    $form['autoassignrole_user_selection'] = [
      '#type' => 'radios',
      '#title' => $this->t('Selection method'),
      '#default_value' => $config->get('autoassignrole_user_selection', AUTOASSIGNROLE_ELEMENT_RADIO_CHECKBOX),
      '#description' => $this->t('The type of form elements the end user will be presented with.'),
      '#options' => [
        AUTOASSIGNROLE_ELEMENT_RADIO_CHECKBOX => $this->t('Radio Buttons/Checkboxes'),
        AUTOASSIGNROLE_ELEMENT_SELECT => $this->t('Selection Box'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="autoassignrole_user_active"]' => ['value' => 1],
        ],
      ],
    ];

    $form['autoassignrole_user_required'] = [
      '#type' => 'radios',
      '#title' => $this->t('Required'),
      '#default_value' => $config->get('autoassignrole_user_required', 0),
      '#description' => $this->t('Should the end user be required to choose a role?'),
      '#options' => [0 => $this->t('No'), 1 => $this->t('Yes')],
      '#states' => [
        'visible' => [
          ':input[name="autoassignrole_user_active"]' => ['value' => 1],
        ],
      ],
    ];

    $form['autoassignrole_user_sort'] = [
      '#type' => 'radios',
      '#title' => $this->t('Sorting'),
      '#default_value' => $config->get('autoassignrole_user_sort', 'SORT_ASC'),
      '#description' => $this->t('Default sort order of roles the user will see.'),
      '#options' => [
        'SORT_ASC' => $this->t('Ascending'),
        'SORT_DESC' => $this->t('Descending'),
        'SORT_WEIGHT' => $this->t('Weight of field'),
      ],
      '#states' => [
        'visible' => [
          ':input[name="autoassignrole_user_active"]' => ['value' => 1],
        ],
      ],
    ];

    $form['autoassignrole_user_description'] = [
      '#type' => 'text_format',
      '#title' => $this->t('User Role Description'),
      '#description' => $this->t('The description displayed to the end user when they are selecting their role during registration.'),
      '#default_value' => $this->t('Select a role'),
      '#required' => FALSE,
      '#format' => filter_default_format(),
      '#prefix' => '<div id="autoassignrole_user_description_wrapper">',
      '#suffix' => '</div>',
      '#states' => [
        'visible' => [
          ':input[name="autoassignrole_user_active"]' => ['value' => 1],
        ],
      ],
    ];

    $form['autoassignrole_user_fieldset_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Role Fieldset Title'),
      '#description' => $this->t('The title of the fieldset that contains role options.'),
      '#default_value' => $config->get('autoassignrole_user_fieldset_title', $this->t('User Roles')),
      '#size' => 60,
      '#maxlength' => 128,
      '#states' => [
        'visible' => [
          ':input[name="autoassignrole_user_active"]' => ['value' => 1],
        ],
      ],
    ];

    $form['autoassignrole_user_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Role Title'),
      '#description' => $this->t('The title of the field that contains the role options the end user sees during registration.'),
      '#default_value' => $config->get('autoassignrole_user_title', $this->t('Role')),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name="autoassignrole_user_active"]' => ['value' => 1],
        ],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('autoassignrole_user_settings.config');
    $values = $form_state->getValues();
    $config->set('autoassignrole_user_active', $values['autoassignrole_user_active'])
    ->set('autoassignrole_user_roles', $values['autoassignrole_user_roles'])
    ->set('autoassignrole_user_multiple', $values['autoassignrole_user_multiple'])
    ->set('autoassignrole_user_selection', $values['autoassignrole_user_selection'])
    ->set('autoassignrole_user_required', $values['autoassignrole_user_required'])
    ->set('autoassignrole_user_sort', $values['autoassignrole_user_sort'])
    ->set('autoassignrole_user_description', $values['autoassignrole_user_description'])
    ->set('autoassignrole_user_fieldset_title', $values['autoassignrole_user_fieldset_title'])
    ->set('autoassignrole_user_title', $values['autoassignrole_user_title'])
    ->save();

    parent::submitForm($form, $form_state);
  }
}

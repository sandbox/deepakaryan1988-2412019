<?php

namespace Drupal\autoassignrole\Controller;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;

class AutoassigneroleAutomatic extends ConfigFormBase {

  public function getFormId() {
    return 'autoassignrole_auto_settings_config_form';
  }

  protected function getEditableConfigNames() {
    return ['autoassignrole_auto_settings.config'];
  }
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('autoassignrole_auto_settings.config');

    $form['autoassignrole_auto_active'] = [
      '#type' => 'radios',
      '#title' => $this->t('Automatic role assignment'),
      '#default_value' => $config->get('autoassignrole_auto_active', 0),
      '#description' => $this->t('Automatic role assignment occurs when the user first logins to the account. This happens without the users knowledge. Set to Enabled to allow this functionality or Disabled to not allow.'),
      '#options' => [1 => t('Enabled'), 0 => t('Disabled')],
    ];

    $form['autoassignrole_admin_active'] = [
      '#type' => 'radios',
      '#title' => $this->t('Automatic role assignment for admin created accounts'),
      '#default_value' => $config->get('autoassignrole_admin_active', 0),
      '#description' => $this->t('Automatic role assignment occurs when the user first logins to the account. This happens without the users knowledge. Set to Enabled to allow this functionality or Disabled to not allow.'),
      '#options' => [1 => t('Enabled'), 0 => t('Disabled')],
      '#states' => [
        'visible' => [
          ':input[name="autoassignrole_auto_active"]' => ['value' => 1],
        ],
      ],
    ];

    // We can disregard the authenticated user role since it is assigned to each
    // user by Drupal.
    $roles = user_role_names(TRUE);
    unset($roles[DRUPAL_AUTHENTICATED_RID]);

    if ($roles) {
      $form['autoassignrole_auto_roles'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('Roles'),
        '#default_value' => $config->get('autoassignrole_auto_roles'),
        '#description' => $this->t('Check the specific roles the user will automatically be assigned to when created by an administrator or through the new user registration process. The Authenticated User role is automatically assigned by Drupal core and can not be edited.'),
        '#options' => $roles,
        '#states' => [
          'visible' => [
            ':input[name="autoassignrole_auto_active"]' => ['value' => 1],
          ],
        ],
      ];
    }
    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->getEditable('autoassignrole_auto_settings.config');
    $values = $form_state->getValues();
    $config->set('autoassignrole_auto_active', $values['autoassignrole_auto_active'])
      ->set('autoassignrole_admin_active', $values['autoassignrole_admin_active'])
      ->set('autoassignrole_auto_roles', $values['autoassignrole_auto_roles'])
      ->save();
    parent::submitForm($form, $form_state);
  }
}
